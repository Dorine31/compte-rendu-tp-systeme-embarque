# SYSTÈMES EMBARQUES


## Préambule
Il est important de savoir manipuler des bits avant de commencer les TP.


Le langage C définit six opérateurs permettant de manipuler les bits :
- l’opérateur « et » : & ;
- l’opérateur « ou inclusif » : | ;
- l’opérateur « ou exclusif » : ^ ;
- l’opérateur de négation ou de complément : ~ ;
- l’opérateur de décalage à droite : >> ;
- l’opérateur de décalage à gauche : <<.


les tables de vérité permettent d'effectuer des calculs sur les bits : 
|Bit 1| Bit 2| 	Opérateur « et »| 	Opérateur « ou inclusif »| 	Opérateur « ou exclusif »|
|:---:|:---:|:---:|:---:|:---:|
|0| 0| 	0| 	0| 	0|
|1 |0| 	0| 	1| 	1|
|0 |1| 	0| 	1| 	1|
|1 |1| 	1| 	1| 	0|


__Systeme binaire__

1 octet : 8 bit

|2⁷|2⁶|2⁵|2⁴|2³|2²|2¹|2⁰|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|128|34|32|16|8|4|2|1|


total : 256 caractères


__Exemples de manipulation de bits :__

```
int a = 0x63; /* 0x63 == 99 == 0b 0110 0011 */
int b = 0x2A; /* 0x2A == 42 == 0b 0010 1010 */
unsigned a = 0x7F; /* 0111 1111 */

/* 0110 0011 & 0010 1010 == 0010 0010 == 0x22 == 34 */
/* 0110 0011 | 0010 1010 == 0110 1011 == 0x6B == 107 */
/* 0110 0011 ^ 0010 1010 == 0100 1001 == 0x49 == 73 */
/* ~0111 1111 == 1000 0000 */
```


__Exemples de décalage de bits :__ 

``` 
var |= (1 << 2) // mettre le bit 2 à 1
var &= ~(1 << 3) // mettre le bit 3 à 0
``` 


__Architectture de l'arduino Uno__

Il faut connaitre l'architecture de l'arduino pour réaliser les TP1 et TP2 correctement.

- Programmation Bare metal
- Microcontrolleur : Atmega328P 8-bit AVR
- Fréquence : 16 Mhz
- Mémoire flash : 32 Ko
- Mémoire RAM : 2 Ko


__Architecture du raspberry pi__

Concernant le TP3, nous devons connaitre l'architecture du raspberry pi 3.

- Programmation linux embarqué
- Microcontrolleur : ARM cortex A53 (ARMv8) 64-bit
- Fréquence : 1,4 GHz
- Mémoire flash : SD
- Mémoire RAM : 1 Go


## Introduction
Les différents TP ont été effectués sur une machine virtuelle préparée à l'avance par l'intervenant. 

> Téléchargement de la VM à l’adresse suivante : < https://transfer.operalink.fr/n6duLo/TP-Embarque-EPSI.ova > et import dans Virtualbox


## Comment procéder pour utiliser la VM ?

Lancer la VM "TP-embarque-EPSI" en cliquant sur le bouton vert "Démarrer". <img src="./images/demarrer.jpeg" width="24" height="24">
Si tout fonctionne correctement, Linux mint doit s’ouvrir.  
Ouvrir VSCodium (IDE pour l’embarqué) <img src="./images/vscodium.png" width="24" height="24">


1. **Pour créer un nouveau projet :**

	- cliquer sur l’icone <img src="./images/maison.png" width="24" height="24"> afin d'ouvrir PlatformIO (environnement de developpement pour l'embarqué)

	- cliquer sur New Project 

	- saisir le nom du projet, sélectionner un board (Arduino Uno par exemple)


2. **Pour ouvrir un projet :**

	- cliquer sur <img src="./images/maison.png" width="24" height="24"> pour ouvri PlatformIO

	- cliquer sur Open Project => Projects => NomDuProjet => Open...

				OU 

	- Ouvrir un dossier => documents => platformIO => projects => NomDuProjet


## PlatformIO
> [Documentation platformIO](https://docs.platformio.org/en/latest/)


PlatformIO permet de faciliter le développement en embarqué.

Avantages d'utiliser platformio :
- gestionnaire de librairies
- platformio.ini contient les configurations
- PIO debug
- Mises à jour
- etc.



<img src="./images/platform.png" width="500">



## VSCodium

L'IDE VSCodium est un fork de vscode. Il fonctionne de la même façon, avec toutes les fonctionnalités et les supports présents pour vscode.

> [Documentation vsCode](https://code.visualstudio.com/docs)
<br />
> [Quelques infos supplémentaires sur vscodium](https://franckchambon.github.io/ClasseVirtuelle/NSI/5-%C3%89diteurs/vscodium.html)



## Structure d’un projet :

 <img src="./images/structure-dossier.png" width="500">

- Le fichier principal se nomme main.cpp, et est situé dans le dossier src

- la fonction setup() permet l’initialisation. Elle n'est appelée qu'une seule fois.

- la fonction loop() contient le code applicatif. Elle est appelée à l'infini après appel de la fonction setup()


## Liens vers les différentes documentations utilisées :

Nous nous sommes appuyés sur différentes documentations pour réaliser nos TP. Voici les liens directs.

> [Fonctions de référence Arduino](https://www.arduino.cc/reference/en/)  

> [Architecture du microcontrôleur de l’Arduino Uno](https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf)  

> [Structure de la carte Arduino / connecteurs](https://docs.arduino.cc/hardware/uno-rev3) (cliquer sur pinout)  

> [Documentation du capteur](http://cdn.sparkfun.com/datasheets/Sensors/Weather/RHT03.pdf)



## TP 1 - BLINK

[source gitlab - Blink](https://gitlab.com/Dorine31/blink)

> Objectif : 
> Faire clignoter une led  
>    • en utilisant les fonctions builtIn,  
>    • puis en les remplaçant par nos propres fonctions.
>  et envoyer un message toutes les secondes  


<u>**Création du projet :**</u> 

<span style="color:green">*New Project => Blink => Arduino Uno => Arduino Framework => Finish*</span>


### Etape 1 : 

> Initialisation de la broche 13 ou LED_BUILTIN de l’Arduino et modification de l’état de la led


I. <u style="font-size: 15px">**Configuration de la broche et changement de l’état de la led**</u>  

**Fonctions de référence dans l’onglet Digital I/O :**

_Les fonctions sont visibles dans la documentation (cf. lien au-dessus) "Fonctions de références"._ 

<u>Pour l’initialisation</u> => pinMode() 

```  
pinMode(LED_BUILTIN, OUTPUT);
```  

<u>Pour la modification de l’état</u>  => digitalWrite() et delay()  
``` 
 digitalWrite(LED_BUILTIN, HIGH);  
 delay(1000);
 digitalWrite(LED_BUILTIN, LOW); 
 delay(1000);
 ``` 
 
Cliquer sur ✔ pour compiler


II. <u style="font-size: 15px">**Test :**  </u>

> Branchement de l’arduino sur le PC avec un cable usb  
> Sélectionner Périphériques => usb => arduino  
> cliquer sur → pour lancer le programme  


III. <u style="font-size: 15px">**Envoyer un message toutes les secondes**</u>  

**Fonctions de référence dans l’onglet Communication :**  

<u>Pour l’initialisation</u> => begin(speed)  

```  
Serial.begin(9600)
 ``` 

<u>Pour afficher des messages</u>  => print() ou println()  

``` 
Serial.println("Blink"); 
```  

Cliquer sur ✔ pour compiler


IV. <u style="font-size: 15px">**Test**</u> (cf. étape 2)


### Etape 2 :  

> Remplacer la fonction d’initialisation builtIn pinMode(13, OUTPUT) par une fonction custom. 

Pour cela il faut trouver à quoi correspond la broche 13. 


I. <u style="font-size: 15px">**visualisation de la carte Arduino.**</u>  

Nous avons recherché dans la documentation (cf. ci-dessus) "Structure de la carte Arduino".

> => sur le schéma, la fiche 13 correspond à PB5 (PortB bit 5)


II. <u style="font-size: 15px">**recherche sur la documentation du microcontrolleur de l’arduino**</u>  

Nous avons recherché dans la documentation (cf. ci-dessus) "Architecture du microcontrolleur Arduino Uno" : page 59 – schema fonctionnel des ports

>> configuration : **« the DDxn bits are accessed at the DDRx I/O** address, **the PORTxn bits at the PORTx I/O address**, and the **PINxn bits at the PINx I/O address. »**

>> The DDxn bit in the DDRx register selects the direction of this pin. If **DDxn** is written logic **one**, Pxn is configured as an **output** pin. 

"Architecture du microcontrolleur Arduino Uno" :  page 280 – register summary
Recherche dans le tableau : 
>> => registre de direction DDRB à l’adresse 0x04
>> => registre PORTB (PortB5) à l’adresse 0x05
>> => registre PINB à l’adresse 0x03

Il ne reste plus qu’à créer une fonction qui prend en compte DDRB et qui initialise le bit 5 à 1 pour le configurer en sortie.

Ecriture du code :  

``` 
define LED_MASK (1 << 5)
 void led_setup() { 
 DDRB |= LED_MASK
}
```

### Etape 3 :

> Configurer l’état de sortie en remplacement de la fonction builtin DigitalWrite()

"Architecture du microcontrolleur Arduino Uno" : page 59 – schema fonctionnel des ports
> « if **PORTxn** is written **logic one** when the pin is configured as an **output pin**, the port pin is driven **high** (one). If PORTxn is written logic **zero** when the pin is configured as an **output pin**, the port pin is driven **low** (zero). »**

**<u>Traduction</u> :** Point haut : port à 1 ; point bas : port à 0. 
On crée donc 2 fonctions led_on() et led_off() et on les utilise dans la fonction loop().  

Ecriture du code :

```
 void led_on() { 
 PORTB |= LED_MASK; // Mise à 1 => high
 }
 void led_off() {
 PORTB &= ~LED_MASK; // Mise à 0 => low
 }
 void loop () {
 led_on();
 delay(1000);
 led_off(); 
 delay(1000); 
 }
```


### Etape 4 :
> Ecrire une fonction toggle pour optimiser le code

"Architecture du microcontrolleur Arduino Uno" : page 60 : « Toggling the Pin

> Writing a logic **one to PINxn**, **toggles** the value of **PORTxn**, i »

<u>**Traduction:**</u> on écrit une fonction toggle() qui fait basculer l'état à 1 sur un registre independant du registre portB.

Ecriture du code :

``` 
void led_toggle() {
PINB = LED_MASK;  
}

void loop() {  
led_toggle();  
delay(1000);  
 }  
```


### Etape 5 :

> Remplacer la fonction delay()

Pour cela, on utilise un autre mécanisme : le timer ; c’est un péripherique du microcontrolleur.
La vitesse du timer (à implémenter) définit une période. A chq période, il y a une incrémentation (+ 1) jusqu’au max. Puis il reprend à zero.

*Dans la documentation "Architecture du microcontrolleur Arduino Uno", il est indiqué que le timerO est déjà utilisé par Arduino. On utilisera donc le **timer1**.*

A partir de la page 89 - **16-bit Timer/Counter1 with PWM**

> Nous devons commencer par **configurer la clock**
> Le timer 1 fait 16 bit => le maximum est donc égal à 2^16 soit **n = 65536**  
> n est stocké dans timer counter "TCNTn"

> Autre info, **notre arduino est sur 8 bits**, **2 registres** seront nécessaires donc contenir notre **valeur de comparaison**. Il faudra faire un décalage du poids fort vers le poids faible pour ajouter les 2 registres cad ``n |= OCT_FORT<< 8 + OCT_FAIBLE``.

> il faut aussi définir le **mode** : ici, une incrémentation.

> Enfin, il ne restera plus qu'à **activer une interruption** lorsqu'on souhaite modifier la led.

<u>Configurations à mettre en place :</u>
- la clock / prescaler
- le seuil (valeur de comparaison)
- le mode
- l'activation de l'interruption


I. <u style="font-size: 15px">**Configurer la clock**</u>  

"Architecture du microcontrolleur Arduino Uno" : page 90 - Notion de prescaler

> **The Timer/Counter can be clocked internally, via the prescaler, or by an external clock source on the T1 pin...**

"Architecture du microcontrolleur Arduino Uno" : page 114 - Timer/Counter0 and Timer/Counter1 Prescalers

> " The prescaled clock has a **frequency** of either f CLK_I/O /8,
f CLK_I/O/64, fCLK_I/O/256, or f CLK_I/O/1024 " 

> "The number of system clock cycles from when the
timer is enabled to the first count occurs can be from 1 to N+1 system clock cycles, where **N equals the prescaler divisor** (8,
64, 256, or **1024**)."

<u>Calcul du prescaler :</u>

Fréquence (nombre de périodes dans 1 seconde (1 mhz => 10⁶)) de l'arduino Uno : 16 mhz => 16 * 10⁶
et fréquence = 1 / période ou **période = 1 / fréquence** 

=> période = 1 / (16 * 10⁶)

maximum de la clock 16bit => 2^16 = 65536

temps max = période x max = 1 / (16 * 10⁶) * 65536 = 0,004095938 (4 millisecondes)

Nous devons augmenter le temps max au-delà de 1seconde pour voir clignoter la led :  
temps max / 1024 => 4 secondes environ (c'est suffisant)

Mais on doit activer une interruption au bout d'une seconde :
**prescaler** = fréquence / 1024 = (16 * 10⁶) / 1024 **= 15625**

> prescaler = 15625

<u>Sur quel registre ?</u>
Le "clock select" n'apparait que sur le registre TCCR1B (cf. page 110) - Timer/Counter1 Control Register B

> registre = TCCR1B

<u>Quelle manipulation ?</u>

"Architecture du microcontrolleur Arduino Uno" : page 110 - Timer/Counter1 Control Register B => Bit 2:0 – CS12:0: **Clock Select**

|CS12 | CS11 | CS10 | Description |
|:---:|:----:|:----:|:-----------:|
|  1  | 0    |1  |clk I/O/1024 (from prescaler)|

> Mettre à 1 les bits 0 et 2 de CS1


II. <u style="font-size: 15px">**configurer le seuil**</u>

"Architecture du microcontrolleur Arduino Uno" : page 97 : "The 16-bit comparator continuously **compares TCNT1 with** the output compare register **OCR1x**".

"Architecture du microcontrolleur Arduino Uno" - page 100 : 
"In **clear timer** on compare or CTC mode **(WGM13:0 = 4 or 12)**, the **OCR1A** or ICR1 register are used to manipulate the
counter resolution".

<span style="color:red">Attention, nous devons configurer 2 registres, car l'arduino est sur 8bits.</span>

"Architecture du microcontrolleur Arduino Uno" : Page 111 :  OCR1AH and OCR1AL – Output Compare Register 1 A

> registres : OCR1AH et OCR1AL ou OCR1A

<u>Manipulations ?</u>

OCR1AL = (uint8_t) (15625 & 0xFF)  

car :
|15625|0b 0011 1101 0000 1001|
|:---:|:---:|
|& 0xFF| 0b 0000 0000 1111 1111|
|=|0b 0000 0000 0000 1001|

OCR1AH = (uint8_t) (15625 & (0xFF << 8)) >> 8 

car :
|15625|0b 0011 1101 0000 1001|
|:---:|:---:|
|& (0xFF << 8)) >> 8|0b 1111 1111 0000 0000|
|=|0b 0011 1101 0000 0000|

Ou simplement OCR1A = 15625

> OCR1A = 15625


III. <u>**Configurer le mode**</u>

"Architecture du microcontrolleur Arduino Uno" : page 109 - Waveform Generation Mode Bit Description

> Registres : 
TCCR1B (WGM13, WGM12 bits 3 et 4) et TCCR1A(WGM11, WGM10 bits 0 et 1)

|Mode| WGM13|WGM12|WGM11|WGM10|Timer/Counter Mode of operation| TOP|Update of OCR1x at|TOV1 Flag|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|4| 0|1|0|0| CTC| OCR1A |Immediate |MAX|

> Mise à 1 du bit 2 de WGM1
> Mise à 0 des bits 3 de WGM1, 1 et 0 de WGM1


IV. <u>**Générer une interruption**</u>
page 100 :

"**An interrupt** can be generated at each time the counter value reaches the **TOP value**"

"Architecture du microcontrolleur Arduino Uno" : page 88 - TIMSK0 – Timer/Counter Interrupt Mask Register

> registre : TIMSK0

"Architecture du microcontrolleur Arduino Uno" - page 112 : "**OCIE1A**: Timer/Counter1, Output Compare A Match Interrupt Enable
When this bit is written to **one**, and the I-flag in the status register is set (interrupts globally enabled), **the Timer/Counter1
output compare A match interrupt is enabled**. "
cf. II. configurer le seuil (OCR1A).

<u>Manipulations ?</u>

TIMSK1:

|7|6|5|4|3|2|1|0|
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
|–| –| ICIE1| –| –| OCIE1B| OCIE1A| TOIE1| 

> Mise à 1 du bit 1 (OCIE1A)

<u>**Fonction d'interrupt :**</u> 

"Architecture du microcontrolleur Arduino Uno" - page 49 : Interrupt vectors (cf. tableau => appel de fonctions builtin)

|Vector No.| Program Address| Source| Interrupt  Definition|
|:---:|:---:|:---:|:---:|
|12|0x0016|<span style="color: blue">TIMER1 COMPA</span> |**Timer/Counter1** compare **match A**|

Ecriture du code :

- Création d'une fonction timer_setup() qui implémente les différents registres  

```timer_setup(){ 
// WGM11 et WGM10 à 0   
TCCR1A = 0;  
// WGM12 à 1, CS10 à 1, CS12 à 1, WGM13 à 0, CS11 à 0      
TCCR1B = ((1 << CS10) | (1 << CS12)) | (1 << WGM12); 
OCR1A = 15625;
TIMSK1 = (1 << OCIE1A);
}
```

Ecriture du code :

- Création d'une fonction ISR() qui appelle la fonction toggle()
La fonction ISR() est appelée à l'interruption, grâce au vecteur d'interruption "TIMER1_COMPA_vect"

``` 
ISR(TIMER1_COMPA_vect){
  led_toggle()
}
```

- On peut maintenant vider la fonction loop() et modifier la fonction setup()

> /* appel de led_setup() et timer_setup()
à l'init ou après une interruption*/

``` 
void setup() {
  led_setup();
  timer_setup();
}
```
> /* le processeur n'a plus besoin de boucler pour vérifier delay(). 
Il attend juste une interruption.*/
``` 
void loop() {}
```


### Etape 6 :

<u>De quoi à besoin le timer pour compter ?</u>
D’une horloge à une certaine fréquence.

<u>A partir de quel moment il commence à compter ?</u>
À la definition du clock select

<u>Mise en pratique :</u> 

> Voir le changement de comportement quand on arrete le timer manuellement

Ecriture du code :

``` 
#define CS_PRESCALER_1024 ((1 << CS10) | (1 << CS12)) 
void loop() {
// Mise à zéro du prescaler après un delai de 10s
delay(10000);
TCCR1B &= ~(CS_PRESCALER_1024);
delay(10000);
// On relance le prescaler après un delai de 10s
TCCR1B |= CS_PRESCALER_1024;
}
```


## TP 2 - DHT22

[source gitlab - dht22](https://gitlab.com/Dorine31/dht22)

> Objectif : 
> Utiliser un capteur d’humidité et de température  
 

<u>**Création du projet :**</u> 

<span style="color:green">*New Project => DHT22 => Arduino Uno => Arduino Framework => Finish*</span>


### Etape 1 : Compréhension

<u>**Comment le capteur transmet les données ?**</u>
Documentation du capteur : page 3 - 16 bits RH data (humidité) - 16 bits T data (Température) - 8 bits check sum (vérifications)

<u>**Comment il nous envoie les 40 bits ?**</u>
cf. schema page 3 - See below figure for overall communication process, the interval of whole process must beyond 2 seconds.

![schema dht22](/images/schema-dht22.png)

> Le protocole 1-Wire permet de connecter des composants entre eux. L'information est codée grâce à la longueur des impulsions.

> Séquence que le microcontrolleur doit respecter pour recevoir les données : un 0 pendant minimum une milliseconde et un 1 entre 20 et 40 microsecondes.

> On différencie les 0 et les 1 avec la durée de l’état haut

> ensuite, le capteur prend le relais. Il met 1 zero pendant 80 microsecondes puis envoi des données au format ci-dessus

<u>Configurations à réaliser :</u> 
- Etat de la broche en output pour envoyer des messages  
DDRB => pinMode(x, output)  
- Configuration du port à 0  
DigitalWrite(x, low)  
- délai 1 milliseconde 
- Ecouter état haut ou bas (vérification du bon fonctionnement)
- Configuration du port à 1 pendant 20 à 30 microsecondes
- Etat de la broche en intput pour écouter les messages


I. <u style="font-size: 15px">**Configurer l'état de la broche en output**</u>  

page 59 de la documentation du microcontrolleur : 
> « If DDxn is written logic **one**, Pxn is configured as an **output** pin.»

> Etat = sortie si 1

Puis on vérifie que tout est ok (etat bas pendant 80 microsec). Le pin nous sert à lire l’état de la broche
la broche 0 du port B a une particularité : elle enregistre la valeur du timer.

Ecriture du code :
_Attention les fonctions ne sont pas à mettre dans le setup mais dans la loop car il faut relancer les fonctions pour une mise à jour de la température_

```void dht22_send_start(){
//1. mode output pour envoyer un 0 et un 1
DDRB |= (1 << DDB0); //pinMode(x, OUTPUT);
PORTB &= ~(1 << DDB0); //digitalWrite(x, low);
delay(2000);
PORTB |= (1 << DDB0); //digitalWrite(x; high)
delayMicroseconds(30);
//2. bascul en input pour écouter les messages
DDRB &= ~(1 << DDB0); //pinMode(x, INPUT);
}

void dht22_receive_ack() {
// Tant que PINB est à 1 on vérifie : on crée 1 timestamp de départ
while(PINB & (1 << PINB0));
unsigned long start = micros();
// tant que PINB est à 0 on vérifie : on crée 1 timestamp de fin
while(!(PINB & (1 << PINB0)));
unsigned long end = micros();
}
```


## TP 3 - Rasberry pi 3 - Création d'un firmware

[Source gitlab : creation de firmware avec buildroot](https://gitlab.com/Dorine31/creation-de-firmware-avec-buildroot)

> Objectif :
> Utilisation de l'outil buildroot (logiciel libre sous licence GPL) pour construire un système embarqué personnalisé.

> Création d'un firmware minimaliste, en taille, en ergonomie. On l'appelle aussi toolchain de cross-compilation.

_La toolchain (le compilateur) est un fichier, qui s'appelle une image, et contient les éléments nécessaires au bon fonctionnement de l'appareil:_

_- un système de fichiers racine (applications et pilotes de l'appareil),_

_- le noyau du système d'exploitation,_

_- des paramètres de configuration (pour notre TP, le fichier raspberrypi3_64_defconfig contenu dans le dossier configs en l'occurence, )_


### Compilation

Buildroot va nous permettre de compiler toutes les briques de bases pour utiliser notre carte.

1. La toolchain ;
2. Le bootloader (si configuré) ;
3. Le noyau Linux ;
4. Le rootfs.

|Source des composants||Buildroot||images distribution|
|:---:|:---:|:---:|:---:|:---:|
|données d'entrée||||éléments produits|
|http, git, .etc| => |Moteur (GNU)| => |bootloader + noyau + rootfs|


### Procédure

1. Télécharger la dernière version de buildroot, directement à partir de la machine virtuelle, déplacer et dezipper le fichier.

[lien vers buildroot](https://buildroot.org/)


2. Installer les librairies obligatoires avant d'installer buildroot : `` sudo apt install libncurses-dev``


3. Se positionner dans le dossier buildroot

> - Taper `` make raspberrypi3_64_defconfig``
> > Ceci permet d'avoir une interface déjà pré-configurée selon les paramètres du raspberrypi 3 - 64bit

> - Taper ``make menuconfig``
> > Ceci donne accès à une interface permettant de configurer la source Linux


4. Utiliser l'interface pour configurer différentes rubriques.

<img src="./images/interface.png" width="800">


### Configuration de l'interface

__Menu Target_architecture :__ 
Menu déjà pré-rempli. On peut s'assurer qu'il contient bien "Aarch64 (little endian)"


__Menu Toolchain :__ 
- toolchain_type => external

Nous aurions pu choisir toolchain internal, afin que Buildroot gère la création et l'utilisation de la toolchain.
Mais nous avons plutot choisi une toolchain déjà compilée.


__Menu System_config :__ 
- hostname => tp-epsi-db
- password => 1234 
- network interface to config through(h0) => supprimer eth0 et créer un fichier de configuration à la place (appelé interfaces)

=> dans root filesystem overlay directories => taper rootf_overlay

Cela permettra de copier le contenu du répertoire dans output/target, qui représente le contenu du root-filesystem.

- Parallèlement, nous allons créer un fichier rootfs_overlay dans le dossier Buildroot. 

Ce dossier contient un dossier etc qui contient : 

>  * un dossier network, qui contient le fichier interfaces. Utiliser nano (après ajout dans le menu packages) pour éditer le fichier et copier-coller :

```
auto lo
iface lo inet loopback
auto eth0
iface eth0 inet dhcp
    pre-up /etc/network/nfs_check
    wait-delay 15
auto wlan0
iface wlan0 inet dhcp
    pre-up wpa_supplicant -B -Dnl80211 -iwlan0 -c/etc/wpa_supplicant.conf
    post-down killall -q wpa_supplicant
    wait-delay 15
iface default inet dhcp 

```


__Menu Kernel :__ 
- kernel => linux kernel


__Menu packages :__ 

Ce menu est fondamental car il définit les différents composants à ajouter au système final.

- development tools 

Ici on configure le ssh ainsi que l'éditeur de texte nano pour éditer du texte si besoin. Utiliser la barre de recherche (taper "/") pour trouver et charger les packages suivants :
> - ssh - dropbrear
> - text editor - nano

De plus, on doit configurer le driver wifi si on veut qu'il soit chargé au boot.

> - driver - rpi-firmwareWifi => brcmfmac studiofirmware

Et pour la connexion wifi : 

> - wifi - wpa_supplicant

[lien informations complémentaires pour configuration wifi](https://blog.crysys.hu/2018/06/enabling-wifi-and-converting-the-raspberry-pi-into-a-wifi-ap/)


- Dans le dossier etc, nous allons créer : 

> * un dossier network et un fichier wpa_supplicant.conf
On utilise nano pour éditer et modifier le fichier : ``nano wpa_supplicant.conf``

```
country=FR
network={
ssid="TP-Embarque"
psk="cletpembarque"
}
```


__Menu filesystem image :__ 

Ce menu permet de sélectionner le format des images à produire. Nous sélectionnons : ext 2/3/4 car la micro-SD est vue comme un disque-dur.

Extended File System est le système de fichiers natif de Linux.


__Menu bootloader :__ 

Dans ce TP, nous ne configurons pas le bootloader mais dans un autre cas, il faudrait le mettre en place.


- Enfin, dans __Buildroot/.../board/rapberrypi3-64/post-build.sh__, ajouter les lignes suivantes : 

```
cp package/busybox/S10mdev ${TARGET_DIR}/etc/init.d/S10mdev
chmod 755 ${TARGET_DIR}/etc/init.d/S10mdev
cp package/busybox/mdev.conf ${TARGET_DIR}/etc/mdev.conf 
```

Cela sert à définir un ensemble de scripts à executer durant la procédure de création de l'image.


### Sauvegarder la configuration et taper ``make`` pour lancer la compilation puis faire un ``sudo apt install build-essential``

- less .config permet de voir ce qui a été configuré dans le fichier (et de naviguer).
```
cat .config | grep -v « ^# » | less
```

Dans le dossier output, plusieurs sous-dossiers sont apparus :

- le dossier build : c'est dans ce dossier que Buildroot compile les différents packages.

- le dossier host : il contient la toolchain

- le dossier images : il contient les images du noyau, du root filesystem, du bootloader (si paramétré), etc. :
bootcode.bin, config.txt, start.elf, etc.

- le dossier staging : c'est un lien symbolique vers la toolchain

- le dossier target : c'est une copie de l'arborescence du root filesystem.

- sdcard.img sera à flasher dans la carte sd